import "./home.scss";
import Input from "../Common/Input/input";
import { useState, useRef, useMemo, useCallback } from 'react';

function Home() {
    const inputRef = useRef([null, null]);
    const [numbers, setNumbers] = useState([0, 0]);

    const handleClick = useCallback((e) => {
        e.preventDefault();
        const value1 = parseFloat(inputRef.current[0].value || 0);
        const value2 = parseFloat(inputRef.current[1].value || 0);
        setNumbers([value1, value2]);
    },[inputRef]
    )

    const memorized = useMemo(() => {
        return numbers[0] + numbers[1];
    }, [numbers]);

    return (
        <div>
            <form className='calc'>
                <Input ref={(el) => (inputRef.current[0] = el)} /><p>+</p>
                <Input ref={(el) => (inputRef.current[1] = el)} />
                <button onClick={handleClick}>sum</button>
            </form>
            <h1>Sum:{memorized}</h1>
        </div>
    );
}

export default Home;


