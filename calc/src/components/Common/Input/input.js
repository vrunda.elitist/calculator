import "./input.scss"
import { forwardRef } from "react"

function Input(props, ref) {
    return (
        <div className="Input">
            <input className='field1' type='number' ref={ref}{...props} />
        </div>
    )
}

export default forwardRef(Input)




























// import React, { useState } from 'react';

// const HomeLoanEMICalculator = () => {
//   const [principal, setPrincipal] = useState('');
//   const [interestRate, setInterestRate] = useState('');
//   const [tenure, setTenure] = useState('');
//   const [emi, setEMI] = useState(null);

//   const calculateEMI = () => {
//     const p = parseFloat(principal);
//     const r = parseFloat(interestRate) / 12 / 100;
//     const n = parseFloat(tenure) * 12;

//     if (p && r && n) {
//       const emiValue = (p * r * Math.pow(1 + r, n)) / (Math.pow(1 + r, n) - 1);
//       setEMI(emiValue.toFixed(2));
//     } else {
//       setEMI(null);
//     }
//   };

//   return (
//     <div>
//       <h1>Home Loan EMI Calculator</h1>
//       <div>
//         <label>Loan Amount ($): </label>
//         <input type="number" value={principal} onChange={(e) => setPrincipal(e.target.value)} />
//       </div>
//       <div>
//         <label>Annual Interest Rate (%): </label>
//         <input type="number" value={interestRate} onChange={(e) => setInterestRate(e.target.value)} />
//       </div>
//       <div>
//         <label>Loan Tenure (years): </label>
//         <input type="number" value={tenure} onChange={(e) => setTenure(e.target.value)} />
//       </div>
//       <button onClick={calculateEMI}>Calculate EMI</button>
//       {emi !== null && (
//         <div>
//           <h2>EMI: ${emi}</h2>
//         </div>
//       )}
//     </div>
//   );
// };

// export default HomeLoanEMICalculator;